/*
 * @Autor: hanjy
 * @Date: 2021-06-27 12:05:41
 * @LastEditors: hanjy
 * @LastEditTime: 2021-06-27 12:12:06
 * @Description: 
 * @FilePath: /react-test/src/first/index.js
 */
function First(props) {
  return (
    <div className="App">
      <h1>hello first</h1>
      <h2>hello first</h2>
    </div>
  );
}
App.defaultProps = {
  name: 'defalutname'
}

export default First;
