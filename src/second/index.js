/*
 * @Autor: hanjy
 * @Date: 2021-06-27 14:15:51
 * @LastEditors: hanjy
 * @LastEditTime: 2021-06-27 14:16:42
 * @Description: 
 * @FilePath: /react-test/src/second/index.js
 */
function Second(props) {
  return (
    <div className="App">
      <h1>second </h1>
    </div>
  );
}
App.defaultProps = {
  name: 'defalutname'
}

export default Second;
