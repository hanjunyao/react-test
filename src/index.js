/*
 * @Autor: hanjy
 * @Date: 2021-05-03 15:37:24
 * @LastEditors: hanjy
 * @LastEditTime: 2021-06-27 15:43:59
 * @Description: 
 * @FilePath: /react-test/src/index.js
 */
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Calculator from './components/Calculator';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <App name="testname1"/>
    <App name="testname2"/>
    <h3>分支1修改5-5</h3>
    <App />
    <Calculator>
      <h3>分支1修改5-1</h3>
      <h3>second</h3>
      <h3>分支2修改5</h3>
      <h3>分支2修改1</h3>
      <h3>分支1修改2</h3>
      <h3>分支1修改3</h3>
      <h1>这是父组件：类似于slot的功能</h1>
    </Calculator>
  </React.StrictMode>,
  document.getElementById('root')
);
// 公共冲突修改second
// <h3>分支1修改5-2</h3>
// 分支1修改3
// 测试公共冲突修改
// 分支1修改1
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
